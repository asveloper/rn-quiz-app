import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Button from 'react-native-button';

export default class Result extends React.Component {

  _handlePress() {
    this.props.handleQuiz(true);
  }

  render() {
    const {result} = this.props;

    return (
      <View style={styles.container}>
        <Text style={styles.result}>{result}</Text>
        <Button
          containerStyle={styles.btnContainer}
          style={styles.btnStyle}
          onPress={() => this._handlePress()}
        >
          Play Again
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  result: {
    fontSize: 56,
    fontWeight: 'bold'
  },
  btnContainer: {
    padding:10,
    height:45,
    overflow:'hidden',
    borderRadius:4,
    backgroundColor: 'white'
  },
  btnStyle: {
    fontSize: 20,
    color: 'green'
  }
});
