import React from 'react';
import { StyleSheet, View } from 'react-native';
import Button from 'react-native-button';

export default class Home extends React.Component {

  _handlePress() {
    this.props.handleQuiz(true);
  }

  render() {
    return (
      <View style={styles.container}>
        <Button
          containerStyle={styles.btnContainer}
          style={styles.btnStyle}
          onPress={() => this._handlePress()}
        >
          Start Quiz
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnContainer: {
    padding:10,
    height:45,
    overflow:'hidden',
    borderRadius:4,
    backgroundColor: 'white'
  },
  btnStyle: {
    fontSize: 20,
    color: 'green'
  }
});
