import React from 'react';

import Home from './Home';
import Quiz from './Quiz';
import Result from './Result';

export default class App extends React.Component {

  state = {
    quiz: false,
  };

  handleQuiz(value) {
    this.setState({quiz: value, result: undefined});
  }

  setResult(result) {
    this.setState({result, quiz: false});
  }

  render() {

    const {quiz, result} = this.state;

    if (!quiz && !result) {
      return <Home handleQuiz={this.handleQuiz.bind(this)}/>;
    }

    if (quiz && !result) {
      return <Quiz setResult={this.setResult.bind(this)} />;
    }

    if (result) {
      return <Result result={result || 0} handleQuiz={this.handleQuiz.bind(this)} />;
    }

  }
}
