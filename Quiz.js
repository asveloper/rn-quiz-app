import React, {PureComponent} from 'react';
import {
  StyleSheet,
  View,
  Alert,
  FlatList,
  Text,
} from 'react-native';
import Button from 'react-native-button';
import striptags from 'striptags';
import { SegmentedControls } from 'react-native-radio-buttons'
import {_} from 'underscore';

export default class Quiz extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      quizzes: [],
      disabled: true,
      results: {},
      radios: []
    };
  }

  componentDidMount() {
    fetch("https://opentdb.com/api.php?amount=10")
      .then(res => {
        if (res && res.ok) {
          const {results} = JSON.parse(res._bodyText);
          this.setState({quizzes: results});
        } else {
          Alert.alert("Error", "Your request cannot be processed at this time!");
        }
      });
  }

  _keyExtractor = ({item, index}) => index;

  renderItem = ({item}) => {
    let answers = _.flatten([[item.correct_answer], item.incorrect_answers]);

    let radios = [];
    answers.map(answer => radios.push({label: striptags(answer), value: answer}));

    return (
      <View style={styles.item}>
        <Text style={styles.question}>{striptags(item.question)}</Text>
        <SegmentedControls
          options={radios}
          onSelection={({value}) => this.handleAnswer(item, value)}
          extractText={(option) => option.label}
          tint={'#f80046'}
          selectedTint= {'white'}
          selectedOption={this.state.results[item.question] ? this.state.results[item.question] : null}
          optionStyle={{fontFamily: 'AvenirNext-Medium'}}
          optionContainerStyle={{flex: 1}}
          direction={"column"}
        />
      </View>
    )
  };

  handleAnswer(item, value) {
    if (!item || !value) {
      return;
    }

    let {results, quizzes} = this.state;
    results[item.question] = value;
    this.setState({results, disabled: quizzes.length == Object.keys(results).length ? false : true});
  }

  handleSubmit() {
    const {results, quizzes} = this.state;
    let count = 0;

    quizzes.map((quiz, index) => {
      if (results[quiz.question] === quiz.correct_answer) {
        count = count + 1;
      }

      if (index === quizzes.length - 1) {
        this.props.setResult(count);
      }
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          style={{backgroundColor: "#fff"}}
          data={this.state.quizzes}
          keyExtractor={this._keyExtractor}
          renderItem={this.renderItem.bind(this)}
        />
        <Button
          containerStyle={styles.btnContainer}
          style={styles.btnStyle}
          disabledContainerStyle={styles.disableBtn}
          onPress={() => this.handleSubmit()}
          disabled={this.state.disabled}
        >
          Submit
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 40,
    marginBottom: 50,
    backgroundColor: '#fff',
  },
  item: {
    width: "100%",
    minHeight: 40,
    padding: 10
  },
  question: {
    fontSize: 16,
    width: "100%",
    fontWeight: 'bold',
    marginBottom: 10,
  },
  btnContainer: {
    padding:10,
    minHeight:45,
    overflow:'hidden',
    borderRadius:4,
    backgroundColor: '#2196f3'
  },
  btnStyle: {
    fontSize: 20,
    color: '#fff'
  },
  disableBtn: {
    backgroundColor: 'grey'
  }
});
